#ifndef BST_HPP
#define BST_HPP
#include "BSTNode.hpp"
#include "BSTIterator.hpp"
#include <iostream>

template<typename Data>
class BST {

protected:

  /** Pointer to the root of this BST, or 0 if the BST is empty */
  BSTNode<Data>* root;
  
  /** Number of Data items stored in this BST. */
  unsigned int isize;

  /** Height of this BST. */
  unsigned int iheight;
  
public:

  /** define iterator as an aliased typename for BSTIterator<Data>. */
  typedef BSTIterator<Data> iterator;

  /** Default constructor.
      Initialize an empty BST.
   */
  BST() : root(0), isize(0), iheight(0) {  }


  /** Default destructor.
      Delete every node in this BST.
   */
  virtual ~BST() {
    deleteAll(root);
  }

  /** Given a reference to a Data item, insert a copy of it in this BST.
   *  Return  true if the item was added to this BST
   *  as a result of this call to insert,
   *  false if an item equal to this one was already in this BST.
   *  Note: This function should use only the '<' or "==" operator when comparing
   *  Data items. (should not use >, <=, >=)
   */
  virtual bool insert(const Data& item) {
    return helperInsert(item, &root);
  }

  /** Find a Data item in the BST.
   *  Return an iterator pointing to the item, or pointing past
   *  the last node in the BST if not found.
   *  Note: This function should use only the '<' or "==" operator when comparing
   *  Data items. (should not use >, <=, >=)
   */ 
  iterator find(const Data& item) const {
    return helperFind(const Data& item, BSTNode *temp);
  }
  
  /** Return the number of items currently in the BST.
   */
  unsigned int size() const {
    helperSize(root);
  }
  
  
  /** Return the height of the BST.
   */ // TODO
  unsigned int height() const {

  }

  /** Return true if the BST is empty, else false.
   */
  bool empty() const {
    if(!root) {
      return true;
    }
    return false;
  }

  /** Return an iterator pointing to the first item in the BST (not the root).
   */ // TODO
  iterator begin() const {

  }

  /** Return an iterator pointing past the last item in the BST.
   */
  iterator end() const {
    return typename BST<Data>::iterator(0);
  }

  /** Perform an inorder traversal of this BST.
   */
  void inorder() const {
    inorder(root);
  }


private:

  /* this is a helper function that recursively finds the item. */
  iterator helperFind(const Data& item, BSTNode *temp) {
    if (!temp) {
      return ;
    } else if(temp->val == item) {
      return ;
    } else if(temp->val < item) {
      helperFind(item, temp->right);
    } else {
      helperFind(item, temp->left);
    }
  }  

  /* this is a helper function that recursively finds the item. */
  unsigned int helperSize(BSTNode *temp) {
    if(!temp) {
      return 0;
    }
    return helperSize(temp->left) + helperSize(temp->right) + 1;
  }

  //I HAVE A QUESTION ON PAPER!
  /**
   *  This is a helper function for insert that takes a pointer to a pointer.
   *  which stores root. This allows the item to be inserted into the BST.
   *  It returns bool value whcih indicates whether the data is inserted.
  */
  bool helperInsert(const Data& item, BSTNode **temp) {
    if(!(*temp)) {
      *temp = new BSTNode (item);
      return true;
    } else if ((*temp)->val == item) {
      return false;
    } else if (item < (*temp)->val) {
      bool inserted = helperInsert(item, (*temp)->left);
      if(!((*temp)->left->parent))
        (*temp)->left->parent = (*temp);
      return inserted;
    } else {
      return helperInsert(item, (*temp)->right);
    } 
  }

  /** Recursive inorder traversal 'helper' function */

  /** Inorder traverse BST, print out the data of each node in ascending order.
      Implementing inorder and deleteAll base on the pseudo code is an easy way to get started.
   */
  /* Pseudo Code:
      if current node is null: return;
      recursively traverse left sub-tree
      print current node data
      recursively traverse right sub-tree
  */   
  void inorder(BSTNode<Data>* n) const {
      
    //to traverse the entire tree inorder, traverse the LST first
    //then traverse the node
    //then traverse the RST. 
    if(!n) {
      return;
    }
    inorder(n->left);
    cout << n->val << " ";
    inorder(n->right);
  }

  /** Find the first element of the BST
   */ 
  static BSTNode<Data>* first(BSTNode<Data>* root) {
    if(root == 0) return 0;
    while(root->left != 0) root = root->left;
    return root;
  }

  /** do a postorder traversal, deleting nodes
   */
  /* Pseudo Code:
      if current node is null: return;
      recursively delete left sub-tree
      recursively delete right sub-tree
      delete current node
    */
    //if we dont delete the node at the end then we lose link to one 
    //or both of its child depending on the way we traverse it
    //this causes memory leaks. Since there is memory allocated for the children
    //and these are still present since the memory was not deallocated.
  static void deleteAll(BSTNode<Data>* n) {
    if(!n) {
      return;
    }
    deleteAll(n->left);
    deleteAll(n->right);
    delete n;
  }

 };


#endif //BST_HPP
