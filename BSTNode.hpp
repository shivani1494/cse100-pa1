#ifndef BSTNODE_HPP
#define BSTNODE_HPP
#include <iostream>
#include <iomanip>

template<typename Data>
class BSTNode {
private:
  BSTNode<Data>* recurseLeft(BSTNode<Data> *leftChild);
  BSTNode<Data>* recurseParent(BSTNode<Data> *node);
public:

  /** Constructor.  Initialize a BSTNode with the given Data item,
   *  no parent, and no children.
   */
  BSTNode(const Data & d) : data(d) {
    left = right = parent = 0;
  }

  BSTNode<Data>* left;
  BSTNode<Data>* right;
  BSTNode<Data>* parent;
  Data const data;   // the const Data in this node.

  /** Return the successor of this BSTNode in a BST, or 0 if none.
   ** PRECONDITION: this BSTNode is a node in a BST.
   ** POSTCONDITION:  the BST is unchanged.
   ** RETURNS: the BSTNode that is the successor of this BSTNode,
     ** or 0 if there is none.
   */
  BSTNode<Data>* successor() {
  	//if right child exists but right child's left child does not
  	if(this->right && !this->right->left) {
  		return right;
  	} else if (this->right && this->right->left) {
  		return recurseLeft(this->right->left);
  	} 
  	//if right child does not exist--  
  	else if (!this->right) {
  		return recurseParent(this);
  	}
  }

  /*finds the leftmost child of the BSTNode passed. */
  BSTNode<Data>* recurseLeft(BSTNode<Data> *leftChild) {
  	if(!leftChild) {
  		return NULL;
  	} else {
  		BSTNode<Data> *child;
  		child = recurseLeft(leftChild->left);
  		if(!child) {
  			child = leftChild;
  		}
  		return child;
  	}
	
  }

  /*finds the parent successor-- by recrusing parent pointers*/
  BSTNode<Data>* recurseParent(BSTNode<Data> *node) {
  	if(!node->parent) {
  		return NULL;
  	} else if (node->parent && node->parent->data < node->data ) {
  		//trace parents if the node is the right child
      recurseParent(node->parent);
  	} else (node->parent && node->parent->data > node->data) {
  		//at some point --hopefully-- if it is not the right most child
      return node->parent;
  	} 
  }

}; 

/** Overload operator<< to print a BSTNode's fields to an ostream. */
template <typename Data>
std::ostream & operator<<(std::ostream& stm, const BSTNode<Data> & n) {
  stm << '[';
  stm << std::setw(10) << &n;                 // address of the BSTNode
  stm << "; p:" << std::setw(10) << n.parent; // address of its parent
  stm << "; l:" << std::setw(10) << n.left;   // address of its left child
  stm << "; r:" << std::setw(10) << n.right;  // address of its right child
  stm << "; d:" << n.data;                    // its data field
  stm << ']';
  return stm;
}

#endif // BSTNODE_HPP
